/*
	Fork.c
		fork the cores for parallel sampling
		(C version only)
		by Thomas Hahn
		last modified 23 Apr 15 th
*/


#define ROUTINE "cubafork"
#include "stddecl.h"

#ifdef HAVE_FORK

#include "sock.h"

#define MINCORES 1

coreinit cubafun_;
extern int cubaverb_;
extern corespec cubaworkers_;

/*********************************************************************/

static inline void Child(int myRank, cint core,dispatch d)
{

    WORKER("running %p on worker %d", d.thisptr, myRank);
    d.worker(myRank, d.thisptr, d.thissize, core);
  
  /*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!    VERIFY LATER IF IT IS NEEDED TO FREE  T    !!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
  //  if( d.thissize ){ 
  //     free(d.thisptr);
  //  }
}

/*********************************************************************/


/*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!    FUNCTION BELLOW HAS BEEN CHANGE TO RECEIVE !!!
 !!!        THE WORKER FUNCTION AS PARAMETER!!     !!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

Extern void SUFFIX(cubafork)(dispatch d)
{
  char out[128];
  int cores, core;
  
  VerboseInit();
 /*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!    BELLOW THE SET BY USER EVIROMENT VARS      !!!
 !!!    HAVE TO BE USE TO CHANGE MPI APP           !!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

  

//#ifdef HAVE_GETLOADAVG
//   if( cubaworkers_.ncores < 0 ) {
//     static int load = uninitialized;
//     if( load == uninitialized ) {
//       double loadavg;
//       getloadavg(&loadavg, 1);
//       load = floor(loadavg);
//     }
//     cubaworkers_.ncores = IMax(-cubaworkers_.ncores - load, 0);
//   }
// #else
 // cubaworkers_.ncores = abs(cubaworkers_.ncores);
// #endif

 // cores = cubaworkers_.naccel + cubaworkers_.ncores;
  // if( cores < MINCORES ) {
  //   *pspin = NULL;
  //   return;
  // }

//   if( cubaverb_ ) {
//     sprintf(out, "using %d cores %d accelerators via "
// #ifdef HAVE_SHMGET
//       "shared memory",
// #else
//       "pipes",
// #endif
//       cubaworkers_.ncores, cubaworkers_.naccel);
//     Print(out);
//   }

//   fflush(NULL);		/* make sure all buffers are flushed,
// 			   or else buffered content will be written
// 			   out multiply, at each child's exit(0) */

  
  /*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!    TEMPORARY MPI INITIALIZIZATION             !!!
 !!!    In the fucture utilize new comunicator     !!!
 !!!    and verify if MPI has been initialized     !!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
  MpiStart();
  cubaworkers_.paccel= 1000;
  cubaworkers_.pcores = 10000;
  cubaworkers_.naccel = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &cubaworkers_.ncores);
  cubaworkers_.ncores--; //excluding master

  int myRank;
  
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  
  printf("MPI worker %d initialized ", myRank);

  //only children will execulte this code
  if( myRank != 0 ) {
      Child(myRank, core, d);
      MpiExit();
      exit(0);
  }
}

/*********************************************************************/

Extern void SUFFIX(cubawait)( )
{
  int cores, core, status;
  

  MasterExit();

  //if( Invalid(pspin) || (spin = *pspin) == NULL ) return;

  //cores = spin->spec.naccel + spin->spec.ncores;

  // for( core = 0; core < cores; ++core ) {
  //   MASTER("closing fd %d", spin->fp[core].fd);
  //   close(spin->fp[core].fd);
  // }

//MPI_Comm_size(MPI_COMM_WORLD, &cores);
printf("Cuba wait function called; \n");

// #ifdef KILL_WORKERS
//   for( core = 1; core < cores; ++core ) {
//     MASTER("Finishing MPI worker %d",core );
//     kill(spin->fp[core].pid, SIGKILL);
//   }
// #endif

  // for( core = 0; core < cores; ++core ) {
  //   DEB_ONLY(pid_t pid;)
  //   MASTER("waiting for child");
  //   DEB_ONLY(pid =) wait(&status);
  //   MASTER("pid %d terminated with exit code %d", pid, status);
  // }

  // free(spin);
  // *pspin = NULL;
}

 #else

 Extern void SUFFIX(cubafork)(Spin **pspin) {}

 Extern void SUFFIX(cubawait)(Spin **pspin){
   MasterExit();
 }

#endif

