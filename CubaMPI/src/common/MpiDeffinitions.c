#include <MpiDeffinitions.h>

void MpiTypeSliceCreate(){
    int blocks[1] = {4};
    MPI_Aint offsets[1] = {0};
    MPI_Datatype types[1] = {MPI_INT};

    MPI_Type_struct(1, blocks, offsets, types, &MPI_Slice);
    MPI_Type_commit(&MPI_Slice);
}

void MpiStart(){
  int initialized;
  
  MPI_Initialized(&initialized);
  if (!initialized){
    MPI_Init(NULL,NULL);
  }

  MpiTypeSliceCreate();
}


void MpiExit(){
 MPI_Barrier(MPI_COMM_WORLD);
 MPI_Finalize();
}
