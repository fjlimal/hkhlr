#ifndef _MPIDEFFNITIONS_H_
#define _MPIDEFFNITIONS_H_

#include <mpi.h>
//Types transfered through MPI messages
MPI_Datatype MPI_Slice; 
MPI_Datatype MPI_DataToProcess;

/*
********************************************
*      MAIN MPI DATA TYPES                 *
********************************************
*/

/*
    In Slice structure there are 4 int fields for the vegas method:
    n: Number of points that must be sent to the accelerators 
    m: Minimun value among number of cores and number of points sent to accelerators
    i: initialized equal zero and it is incremented by the amount of samples sent on each sent in the master. 
       It is returned from the son so the results can be saved on the right array location. 
    iter: number of iterations in the main loop found in "Integrante.c"
*/
void MpiStart();
void MpiTypeSliceCreate();






#endif
