/*
	Parallel.c
		the parallel sampling routine
		for the C versions of the Cuba routines
		by Thomas Hahn
		last modified 23 Apr 15 th
*/

#include "sock.h"

#define MINSLICE 10
#define MINCORES 1
/*#define MINCORES 2*/

typedef struct {
  number n, m, i;
  VES_ONLY(count iter;)
} Slice;


Extern void SUFFIX(cubafork)(dispatch);
Extern void SUFFIX(cubawait)();

/*********************************************************************/

static inline void DoSampleParallel( This *t, number n, creal *x, real *f
  VES_ONLY(, creal *w, ccount iter))
{
  char out[128];
  Slice slice, rslice;
  int  receiverRank, senderRank, abort, running = 0;

  /*
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!    EXCLUDE TMPDATA VARIABLE AFTER TESTS       !!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
  corespec tmpdata;
  
  tmpdata.paccel= 1000;
  tmpdata.pcores = 10000;
  tmpdata.naccel = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &tmpdata.ncores);
  int MPI_CORES = tmpdata.ncores;
  tmpdata.ncores--; //excluding master


  
  cint paccel = tmpdata.paccel;
  cint naccel = IMin(tmpdata.naccel, (n + paccel - 1)/IMax(paccel, 1));
  cnumber nrest = IDim(n - naccel*paccel);
  cint ncores = IMin(tmpdata.ncores, nrest/MINSLICE);
  number pcores = IMin(tmpdata.pcores, nrest/IMax(ncores, 1));
  number nx = nrest - ncores*pcores;
  if( nx >= ncores ){ 
    nx = 0;
  }

  t->neval += n;

  if( VERBOSE > 2 ) {
    sprintf(out, "sampling " NUMBER " points each on %d cores",
      pcores, ncores);
    Print(out);
  }

  slice.n = paccel;
  slice.m = IMax(slice.n, pcores);
  slice.i = 0;
  VES_ONLY(slice.iter = iter;)

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  HERE, IN PUTSAMPLES IS WHERE THE DATA OS SENT TO     !!!
!!!  THE SON.                                             !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

//---------------- BEGIN DEFINE------------------------------------

#define PutSamples(receiverRank) do { \
  slice.n = IMin(slice.n, n); \
  MASTER("sending samples (sli:%lu[+" VES_ONLY(NUMBER "w:%lu+") \
    NUMBER "x:%lu]) to the MPI process %d", \
    sizeof slice, VES_ONLY(slice.n, sizeof *w,) \
    slice.n, t->ndim*sizeof *x,receiverRank); \
    MPI_Request myRequest; \
    MPI_Isend((void * ) &slice , 1, MPI_Slice,receiverRank, MpiTag_SliceFromFather, MPI_COMM_WORLD, &myRequest);\
    /*sending Weigths and X values at once n+n*t->dim [n=size of w vector] and [n*t->dim = size of x vector]*/\
    MPI_Isend((void * )w,slice.n*(1+t->ndim), MPI_Double_Cuba,receiverRank, MpiTag_DataFromFather, MPI_COMM_WORLD, &myRequest);\
    w += slice.n;\
    x += slice.n*t->ndim; \
    slice.i += slice.n; \
    n -= slice.n; \
    ++running; \
} while( 0 )


//---------------- END DEFINE------------------------------------

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! HERE IN GETSAMPLES IS WHERE THE PROCESSED DATA IS     !!!
!!! RECEIVED FROM THESONS IS DONE.                        !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

//---------------- BEGIN DEFINE-----------------------------------
#define GetSamples(senderRank) do { \
  MPI_Recv((void*)&rslice, 1, MPI_Slice,senderRank,MpiTag_SliceFromSon,MPI_COMM_WORLD,NULL);\
  MASTER("reading samples (sli:%lu[+" NUMBER "f:%lu]) from MPI process %d", \
    sizeof rslice, rslice.n, t->ncomp*sizeof *f, senderRank); \
  if( rslice.n == -1 ){ abort = 1; }\
  else \
    MPI_Recv((void*) (f + rslice.i*t->ncomp), rslice.n*t->ncomp,MPI_Double_Cuba, senderRank,MpiTag_DataFromSon,MPI_COMM_WORLD, NULL ); \
  --running; \
} while( 0 )
//---------------- END DEFINE------------------------------------

  
  for( receiverRank = 1; n && receiverRank < MPI_CORES; ++receiverRank ) {
    pcores -= (receiverRank == nx);
    slice.n = (receiverRank < 0) ? paccel : pcores;
   
// PutSamples(receiverRank); ! TEMPRORARY COMENTED    
//---------------- FOR DEBUG PUROPOSE /!\ DELETE WHEN DONE /!\-----------------------------------

do { 
  slice.n = IMin(slice.n, n); 
  MASTER("sending samples (sli:%lu[+" VES_ONLY(NUMBER "w:%lu+") 
    NUMBER "x:%lu]) to the MPI process %d", sizeof slice, VES_ONLY(slice.n, sizeof *w,) 
    slice.n, t->ndim*sizeof *x,receiverRank); 
    MPI_Request myRequest; 
    MPI_Isend((void * ) &slice , 1, MPI_Slice,receiverRank, MpiTag_SliceFromFather, MPI_COMM_WORLD, &myRequest);
    /*sending Weigths and X values at once n+n*t->dim [n=size of w vector] and [n*t->dim = size of x vector]*/
    MPI_Isend((void * )w,slice.n*(1+t->ndim), MPI_Double_Cuba,receiverRank, MpiTag_DataFromFather, MPI_COMM_WORLD, &myRequest);
    w += slice.n;
    x += slice.n*t->ndim; 
    slice.i += slice.n; 
    n -= slice.n; 
    ++running; 
} while( 0 );
//---------------- END DEBUG THING-----------------------------------





  }

  abort = 0;

  while( running ) {
  
  for( receiverRank = 1; receiverRank < MPI_CORES; ++receiverRank ) {     
//  GetSamples(receiverRank);!TEMPORARY COMENTED
  //---------------- FOR DEBUG PUROPOSE /!\ DELETE WHEN DONE /!\-----------------------------------
  int senderRank = receiverRank;
do { 
  MPI_Recv((void*)&rslice, 1, MPI_Slice,senderRank,MpiTag_SliceFromSon,MPI_COMM_WORLD,NULL);
  MASTER("reading samples (sli:%lu[+" NUMBER "f:%lu]) from MPI process %d", sizeof rslice, rslice.n, t->ncomp*sizeof *f, senderRank); 
  if( rslice.n == -1 ){ abort = 1; }
  else 
    MPI_Recv((void*) (f + rslice.i*t->ncomp), rslice.n*t->ncomp, MPI_Double_Cuba, senderRank,MpiTag_DataFromSon, MPI_COMM_WORLD, NULL );
  --running; 
} while( 0 );
  //---------------- END DEBUG THING-----------------------------------
    if( abort ){
      break;
    }
        
	if( n ){ 
	  // PutSamples(receiverRank); ! TEMPRORARY COMENTED    
//---------------- FOR DEBUG PUROPOSE /!\ DELETE WHEN DONE /!\-----------------------------------

do { 
  slice.n = IMin(slice.n, n); 
  MASTER("sending samples (sli:%lu[+" VES_ONLY(NUMBER "w:%lu+") 
    NUMBER "x:%lu]) to the MPI process %d", sizeof slice, VES_ONLY(slice.n, sizeof *w,) 
    slice.n, t->ndim*sizeof *x,receiverRank); 
    MPI_Request myRequest; 
    MPI_Isend((void * ) &slice , 1, MPI_Slice,receiverRank, MpiTag_SliceFromFather, MPI_COMM_WORLD, &myRequest);
    /*sending Weigths and X values at once n+n*t->dim [n=size of w vector] and [n*t->dim = size of x vector]*/
    MPI_Isend((void * )w,slice.n*(1+t->ndim), MPI_Double_Cuba,receiverRank, MpiTag_DataFromFather, MPI_COMM_WORLD, &myRequest);
    w += slice.n;
    x += slice.n*t->ndim; 
    slice.i += slice.n; 
    n -= slice.n; 
    ++running; 
} while( 0 );
//---------------- END DEBUG THING-----------------------------------



	}
      }
    }
  

  if( abort ){ 
	longjmp(t->abort, -99);
  }
#ifdef FRAMECOPY
  if( t->shmid != -1 ){
    Copy(f, t->frame + slice.m*(NW + t->ndim), slice.m*t->ncomp);
  }
#endif
}

/*********************************************************************/

static void DoSample(This *t, number n, creal *x, real *f
  VES_ONLY(, creal *w, ccount iter))
{

    /*
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! ONLY PARALELL SAMPLES WILL BE DONE FOR A WHILE        !!!
    !!! /!\ IT WILL BE NEEDED TO CONSIDER THE SERIAL /!\      !!!
    !!! /!\  VERSION AT THE END                      /!\      !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    */
  // if( t->spin == NULL ||
  //     t->spin->spec.ncores + t->spin->spec.naccel < MINCORES ||
  //     n < MINCORES*MINSLICE )
  //   DoSampleSerial(t, n, x, f VES_ONLY(, w, iter));
  // else
    DoSampleParallel(t, n, x, f VES_ONLY(, w, iter));
}

/*********************************************************************/

static void Worker(int myRank, This *t, const size_t alloc, cint core)
{
  Slice slice;
  /*
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!! HERE THE WORKER RECEIVE THE SLICE DATA FROM MASTER    !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  */
  MPI_Recv(&slice, 1, MPI_Slice,0,MpiTag_SliceFromFather, MPI_COMM_WORLD,NULL);
  if( slice.n != -1 ) {
    if( cubafun_.initfun ) cubafun_.initfun(cubafun_.initarg, &core);
    do {
      number n = slice.n;
      WORKER("received slice.n = " NUMBER, n);
    
      if( n > 0 ) {
        real VES_ONLY(*w,) *x, *f;
        WORKER("reading samples (sli:%lu[+" VES_ONLY(NUMBER "w:%lu+")
          NUMBER "x:%lu]) from MPI worker %d",
          sizeof slice, VES_ONLY(n, sizeof *w,) n, t->ndim*sizeof *x, myRank);

        VES_ONLY(w = t->frame;)
        x = t->frame + slice.m*NW;
        f = x + slice.m*t->ndim;
        {
          // VES_ONLY(
          //   readsock(fd, w, n*sizeof *w);
          //   )

          /*
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!! HERE THE WORKER RECEIVES DATA TO BE PROCESSED         !!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          */
          MPI_Recv(w,n*(t->ndim+1),MPI_Double_Cuba,0,MpiTag_DataFromFather,MPI_COMM_WORLD,NULL);
        }

        slice.n |= SampleRaw(t, n, x, f, core VES_ONLY(, w, slice.iter));
        WORKER("writing samples (sli:%lu[+" NUMBER "f:%lu]) to MPI worker %d",
          sizeof slice, slice.n, t->ncomp*sizeof *f, myRank);
       
        /*
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!     HERE THE WORKER SENDS THE SLICE DATA TO MASTER    !!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          */
        MPI_Send(&slice , 1, MPI_Slice,0, MpiTag_SliceFromSon, MPI_COMM_WORLD);
        if( slice.n != -1 ){
           /*
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          !!!     HERE THE WORKER SENDS THE PROCESSED DATA          !!!
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          */
          MPI_Send(f , slice.n*t->ncomp, MPI_Double_Cuba,0, MpiTag_DataFromSon, MPI_COMM_WORLD);
        }
      }
      
      if(slice.n != -1){
        MPI_Recv(&slice, 1, MPI_Slice,0,MpiTag_SliceFromFather, MPI_COMM_WORLD, NULL);
      }
    } while(slice.n != -1 );

    if( cubafun_.exitfun ) cubafun_.exitfun(cubafun_.exitarg, &core);

    FrameFree(t, Worker);
 
  }

  WORKER("worker wrapping up");
}

/*********************************************************************/

static inline void ForkCores(This *t)
{
  dispatch d;
/*
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!       método worker adicionado em outro lugar !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  */
  d.worker = Worker;
  d.thisptr = t;
  d.thissize = sizeof *t;

 SUFFIX(cubafork)(d);
 //if( t->spin == NULL ) return;
   // d.thissize = 0;
  
}

/*********************************************************************/

static inline void WaitCores(This *t)
{
  // if( Invalid(pspin) ) SUFFIX(cubawait)(&t->spin);
  // else {
    Slice slice = { .n = -1 };
   // cint cores = t->spin->spec.naccel + t->spin->spec.ncores;
    cint mpiWorkers;
    MPI_Comm_size(MPI_COMM_WORLD, &mpiWorkers);
    //const fdpid *pfp = t->spin->fp;
    int receiverRank;
    for( receiverRank = 1; receiverRank < mpiWorkers; ++receiverRank ){
      MPI_Request myRequest; 
      MPI_Isend((void *) &slice , 1, MPI_Slice,receiverRank, MpiTag_SliceFromFather, MPI_COMM_WORLD, &myRequest);
    }
    MasterExit();
    MpiExit();
  // }
}

